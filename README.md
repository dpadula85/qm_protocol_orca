Description
===========
Modules to generate QM input files starting from SMILES string.
The program also submits jobs to a PBS queuing system and parses the
resulting output files after completion.
Potentially, it is generalisable QM software other than ORCA and other
queueing systems.

Protocol
========
- Structure generation.

- MMFF94s conformational analysis, optimisation and alignment of conformers.
The default option only returns the most stable conformer, but the functions
are already such that multiple conformers can be returned.

- Input file writing according to user defined options.

- Submission to PBS according to user defined options.


Instructions
============
Run the program to check for options
    `python main.py -h`

Available options are:

- `--db`: specify the `smiles` file with the molecules to be processed.

- `--qm`: specify the text file with G16 and submission options.

- `-n, --nconfs`: specify the number of conformers for each molecule.

- `-o, --outdir`: specify the output folder

Example files are available in the `data` folder.
