Instructions
============
- `G16_opts.txt`: file with calculation and submission options.

- `mols.smi`: file with the smiles of molecules to be calculated (1 per line).

- `marv.smi`: same as `mols.smi` but without the comment line (to be opened
with MarvinSketch/MarvinView or similar).
