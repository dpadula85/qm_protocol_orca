#!/usr/bin/env python

import os
import sys
import numpy as np
from random import choice
from itertools import groupby
from rdkit import Chem, DataStructs
from rdkit.Chem import rdMolDescriptors


def save_data(data, filename, path=None, hdr=None):

    if path is None:
        path = os.path.join(os.getcwd(), "results")
    else:
        path = os.path.join(os.getcwd(), path, "results")

    try:
        os.makedirs(path)
    except OSError:
        pass

    try:
        ncols = data.shape[1]
    except IndexError:
        ncols = 1

    if hdr is None:
        hdr = [ str(i) for i in range(1, ncols + 1) ]

    # Make filename
    filename = os.path.join(path, filename)

    # Make idxs
    idxs = np.arange(len(data))

    # Make headers and formats
    hdr = [ "Mol" ] + hdr
    hdr_fmt = "\n%3s" + "%12s" * ncols + "\n"
    hdr = hdr_fmt % tuple(hdr)
    fmt = "%5.03d" + "%12.6f" * ncols

    # Merge idxs and data
    tot = np.c_[ idxs, data ]

    # Save
    np.savetxt(filename, tot, fmt=fmt, header=hdr)

    return


def flat2gen(alist):
    for item in alist:
        if isinstance(item, list):
            for subitem in item:
                yield subitem
        else:
            yield item


def compact_extended_list(idxs, factor=0):

    # factor optional parameter to clean out python numeration starting from 0
    compact = []

    for k, iterable in groupby(enumerate(sorted(idxs)), lambda x: x[1] - x[0]):

         rng = list(iterable)

         if len(rng) == 1:

             s = str(rng[0][1] + factor)

         else:

             s = "%s-%s" % (rng[0][1] + factor, rng[-1][1] + factor)

         compact.append(s)

    return compact


def parse_opts(filename):
    '''
    Function to parse a two column file into a dictionary.

    Parameters
    ----------
    filename: str.
        name of the file to be parsed

    Returns
    -------
    opts: dict.
        dictionary containing the options specified in filename.
    '''

    opts = {}
    with open(filename) as f:
        for line in f:

            # Skip comments and empty lines.
            if line.startswith("#") or line.strip() == "":
                continue

            # Strip and convert to lowecase.
            data = line.strip().lower().split()
            try:
                opts[data[0]] = int(data[1])
            except ValueError:
                try:
                    opts[data[0]] += ', ' + ' '.join(data[1:])
                except KeyError:
                    opts[data[0]] = ' '.join(data[1:])

    return opts


def banner(text=None, ch='=', length=78):
    """Return a banner line centering the given text.

        "text" is the text to show in the banner. None can be given to have
            no text.
        "ch" (optional, default '=') is the banner line character (can
            also be a short string to repeat).
        "length" (optional, default 78) is the length of banner to make.

    Examples:
        >>> banner("Peggy Sue")
        '================================= Peggy Sue =================================='
        >>> banner("Peggy Sue", ch='-', length=50)
        '------------------- Peggy Sue --------------------'
        >>> banner("Pretty pretty pretty pretty Peggy Sue", length=40)
        'Pretty pretty pretty pretty Peggy Sue'
    """
    if text is None:
        return ch * length

    elif len(text) + 2 + len(ch)*2 > length:
        # Not enough space for even one line char (plus space) around text.
        return text

    else:
        remain = length - (len(text) + 2)
        prefix_len = remain / 2
        suffix_len = remain - prefix_len

        if len(ch) == 1:
            prefix = ch * prefix_len
            suffix = ch * suffix_len

        else:
            prefix = ch * (prefix_len/len(ch)) + ch[:prefix_len%len(ch)]
            suffix = ch * (suffix_len/len(ch)) + ch[:suffix_len%len(ch)]

        return prefix + ' ' + text + ' ' + suffix


def transl_dict(v):

      if type(v) is str:
        w = v

      elif type(v) is int or type(v) is float:
        w = str(v)

      elif type(v) is list:
        w = ', '.join(map(str, v))

      elif type(v) is dict:
        lenv = len(v)
        w = [None]*lenv
        for kk, vv in v.items():
          w[kk - 1] = transl_dict(vv)

      elif hasattr(v, '__call__'):
        w = v.__name__

      else:
        w = v

      return w


def print_dict(opts_dict, title=None, ch="=", outstream=None):

    if outstream:
      sys.stdout = outstream

    if not title:
      title = "# Options"

    print("# " + banner(ch=ch, length=38))
    print(title)
    print("#")
    fmt = "# %-18s %-20s"
    for k, v in sorted(opts_dict.items()):
        if type(v) in [str, int, float, bool, list] or hasattr(v, '__call__'):
            w = transl_dict(v)
            print(fmt % (k, w))

        elif type(v) is dict:
            print_dict(v, title=k, ch="-")

    print("# " + banner(ch=ch, length=38))
    print

    return


if __name__ == '__main__':
    pass
