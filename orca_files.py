#!/usr/bin/env python

import os
import numpy as np
from itertools import groupby
from rdkit import Chem
from rdkit.Chem.Descriptors import NumRadicalElectrons

au2eV = 27.21138505
au2wn = 219474.63
eV2wn = au2wn / au2eV
wn2eV = 1 / eV2wn

def get_structure(mol, cid=0):
    '''
    Function to obtain the 3D coordinates and atomic numbers of a given
    conformer of a molecule.

    Parameters
    ----------
    mol: RDKit mol object.
        mol object containing conformers for which the structure is desired.
    cid: int (default: 0).
        conformer index of the desired conformer.

    Returns
    -------
    coords: np.ndarray of shape (N, 4).
        structure of the mol object containing N atoms, atomic number, and
        coordinates.
    '''

    coords = []
    conf = mol.GetConformer(cid)

    # Loop over atoms to get their Z and x, y, z.
    for atidx in range(mol.GetNumAtoms()):
        z = mol.GetAtomWithIdx(atidx).GetAtomicNum()
        pos = conf.GetAtomPosition(atidx)
        coord = [ z, pos.x, pos.y, pos.z ]
        coords.append(coord)

    coords = np.array(coords)

    return coords


def write_inputs(mol_objs, cids, opts=None, path=None):
    '''
    Function to write inputs for all the molecules in a list.

    Parameters
    ----------
    mol_objs: RDKit mol object or list of RDKit mol objects.
        list or molecule objects for the molecules to write.
    cids: list of int or list of lists of int.
        conformer indexes for each molecule object in mol_confs.
    opts: dict (default: None).
        dictionary containing the options for the QM calculations to be run.
    path: str (default: None).
        path where the file will be written.

    Returns
    -------
    comnames: list.
        list of generated input files.
    '''

    # Deal with single and iterable
    try:
        N = len(mol_objs)
    except TypeError:
        mol_objs = [ mol_objs ]
        cids = [ cids ]

    # Loop over all molecules
    comnames = []
    for i, mol in enumerate(mol_objs):
        comname = "mol%03d.inp" % i
        comname = write_input(mol, cids[i], opts=opts,
                              filename=comname, path=path)
        comnames.extend(comname)

    return comnames


def write_input(mol, cids, opts=None, filename=None, path=None):
    '''
    Function to write inputs for all the conformers of a single molecule.

    Parameters
    ----------
    mol: RDKit mol object.
        molecule object for the molecule to write.
    cids: list of int.
        conformer indexes for the molecule object.
    opts: dict (default: None).
        dictionary containing the options for the QM calculations to be run.
    filename: str (default: None).
        name of the file to which the input will be written.
    path: str (default: None).
        path where the file will be written.

    Returns
    -------
    fnames: list.
        list of generated input files.
    '''

    if path is None:
        path = os.getcwd()
    else:
        path = os.path.join(os.getcwd(), path)
        try:
            os.makedirs(path)
        except OSError:
            pass

    # Loop over conformers
    fnames = []
    for cid in cids:

        # Create filename containing mol idx and conf idx
        ext = filename.split(".")[-1]
        basename = '.'.join(filename.split(".")[:-1])
        conf_str = "_M%02d" % cid
        basename = basename + conf_str
        fname = basename + "." + ext
        fname = os.path.join(path, fname)

        # Get coords
        coords = get_structure(mol, cid)

        # Get charge and mult
        chg = Chem.GetFormalCharge(mol)
        mult = 2 * NumRadicalElectrons(mol) + 1

        std_keys = [ 'mem', 'time', 'queue', 'program', 'nproc', 'keywords' ]
        blocks = [ k for k in opts.keys() if k not in std_keys ]
        blocks = sorted(blocks, key=lambda x: x[-1])

        jobs = groupby(blocks, key=lambda x: x[-1])

        # Write QM input file
        with open(fname, "w") as f:

            for I, p in enumerate(jobs):
                k, v = p
                job_blocks = list(v)
                job_blocks = [ b.replace(k, "") for b in job_blocks ]

                # Create job string
                job = "! %s" % (' '.join(opts['keywords%s' % k].split(", ")))
                job_blocks = [ x for x in job_blocks if x not in std_keys ]

                # Write standard keywords
                if I > 0:
                    f.write("\n\n")
                    f.write("$new_job\n")

                f.write("%s\n" % job)
                f.write('%%base "%s_job%s"\n' % (basename, k))
                f.write("%%pal nprocs %d end\n" % opts['nproc'])

                # Write blocks
                for job_block in job_blocks:
                    f.write("%%%s\n" % job_block)
                    block_opts = opts["%s%s" % (job_block, k)].split(", ")
                    for block_opt in block_opts:
                        f.write("  %s\n" % block_opt)
                    f.write("end\n")

                if I == 0:
                    # Write geometry
                    f.write("* xyz %d %d\n" % (chg, mult))
                    np.savetxt(f, coords, fmt="% -8d %14.8f %14.8f %14.8f")
                    f.write("*")
                else:
                    f.write("* xyzfile %d %d\n" % (chg, mult))

        fnames.append(fname)

    return fnames


if __name__ == '__main__':

    # Read in the whole library.
    frags = Chem.SmilesMolSupplier('mols.smi')
    fragslist = [ m for m in frags ]

    from gen_coords import *
    tot_confs, cids = gen_coords(fragslist)

    from util import *
    opts = parse_opts("G16_opts.txt")

    comnames = write_inputs(tot_confs, cids, opts=opts)
