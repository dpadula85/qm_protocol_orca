#!/usr/bin/env python

import logging
import numpy as np
from qm_manage import check_done
from cclib.parser import ORCA

au2eV = 27.21138505
au2wn = 219474.63
eV2wn = au2wn / au2eV
wn2eV = 1 / eV2wn

def parse_list(IDs, filenames, parse_fn):
    '''
    Function to parse a list of filenames to extract data, after SGE job
    completion.

    Parameters
    ----------
    IDs: int.
        list of SGE job IDs to be checked.
    filenames: list of str.
        list of filenames to be parsed, generated by the corresponding SGE
        job ID.
    parse_fn: callable.
        function to be called on filename to extract the desired data.

    Returns
    -------
    data: np.array.
        array containing the parsed information. The first dimension will
        have the same shape as IDs or filenames. The second dimension depends
        on the output of parse_fn.
    '''

    data = []
    for i, filename in enumerate(filenames):

        ID = IDs[i]
        done = check_done(ID)

        if done == True:
            datai = parse_fn(filename)

        data.append(datai)

    data = np.array(data)

    return data


def get_scf_energy(filename):
    '''
    Function to extract the SCF energy from a ORCA logfile.

    Parameters
    ----------
    filename: str.
        name of the file to be parsed.

    Returns
    -------
    scfene: float.
        SCF energy at the last SCF loop (in eV).
    '''

    obj = ORCA(filename, loglevel=0)
    data = obj.parse()
    scfene = data.scfenergies[-1] / au2eV

    return scfene


if __name__ == '__main__':

    # Example on mol000_M00

    # Get GS energy
    gs = get_scf_energy("mol000_M00.out")
