#!/usr/bin/env python

import sys
import numpy as np
from rdkit import RDLogger

from opts import *
from util import *


if __name__ == '__main__':

    Opts = options()
    print_dict(Opts)

    # Suppress RDKit warnings.
    lg = RDLogger.logger()
    lg.setLevel(RDLogger.ERROR)

    # Read G16 and submission options
    opts = parse_opts(Opts['QMOpts'])

    # Read in the whole library
    from rdkit import Chem
    frags = Chem.SmilesMolSupplier(Opts['DBFile'])
    fragslist = [ m for m in frags ]

    # Generate molecules
    from gen_coords import *
    tot_confs, cids = gen_coords(fragslist, nconfs=Opts['NConfs'])

    # Write G16 input files
    from orca_files import *

    # GS opt
    opt_comnames = write_inputs(tot_confs, cids, opts=opts, path=Opts['OutDir'])

    # Generate submission script and submit optimisations only
    # Submit other calculations externally
    from qm_manage import *

    # Submit optimisations
    # opt_lognames, opt_IDs = submit_list(opt_comnames, opts)
    opt_lognames, opt_IDs = submit_array(opt_comnames, opts)
    sys.exit()

    # # Save Submitted IDs to a file
    # submitted_IDs = sorted(opt_IDs)
    # np.savetxt("submitted_IDs.txt", submitted_IDs, fmt="%d")

    # Start checking jobs to extract data
    from parse import *
    gs_enes = parse_list(opt_IDs, opt_lognames, get_scf_energy)
    save_data(gs_enes, "gs_enes.dat", path=Opts['OutDir'])
