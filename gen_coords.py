#!/usr/bin/env python

from rdkit import Chem
from rdkit.Chem import AllChem
from collections import OrderedDict


def gen_coords(mol_objs, nconfs=None):
    '''
    Function to generate 3D coordinates for all the molecules in a list.

    Parameters
    ----------
    mol_objs: RDKit mol object or list of RDKit mol objects.
        list of molecule objects to be joined to form the oligomer.
    nconfs: int or list of int (default: None).
        number of conformers required for each molecule. Defaults to 1.

    Returns
    -------
    mol_confs: RDKit mol object or list of RDKit mol objects.
        list of molecule objects containing the desired number of conformers.
    cids: list of int or list of lists of int.
        conformer indexes for each molecule object in mol_confs.
    '''

    try:
        N = len(mol_objs)
    except TypeError:
        mol_objs = [ mol_objs ]
        N = len(mol_objs)

    if nconfs is None:
        nconfs = [ 1 ] * N
    elif type(nconfs) == int:
        nconfs = [ nconfs ] * N

    # Generate conformers for each molecule.
    cids = []
    mol_confs = []
    for i, mol in enumerate(mol_objs):
        mol, confs_dict = gen_confs(mol, nconfs[i])
        mol_confs.append(mol)
        cids.append(confs_dict.keys())

    # Return a single element or more.
    if len(mol_confs) == 1:
        mol_confs = mol_confs[0]
        cids = cids[0]
        return mol_confs, cids
    else:
        return mol_confs, cids


def gen_confs(mol, nconfs=1):
    '''
    Function to generate conformers for a molecule.

    Parameters
    ----------
    mol: RDKit mol object.
        molecule object for the conformational analysis.
    nconfs: int.
        number of conformers desired (default: 1).

    Returns
    -------
    mol: RDKit mol object.
        molecule object with the desired conformers.
    conf_energies: dict.
        dictionary of conformer indexes (keys) and energies (values, KCal/Mol).
    '''

    # Add H atoms.
    mol = Chem.AddHs(mol)

    # Generate conformers.
    cids = AllChem.EmbedMultipleConfs(mol, nconfs, AllChem.ETKDG())

    # Optimise, remove high energy, align by rms.
    mol, conf_energies = opt_confs(mol, list(cids))

    return mol, conf_energies


def opt_confs(mol, cids):
    '''
    Function to optimise conformers for a molecule.

    Parameters
    ----------
    mol: RDKit mol object.
        molecule object containing the conformers to be optimised.
    cids: list of int.
        conformer indexes relative to mol.

    Returns
    -------
    mol: RDKit mol object.
        molecule object with the desired conformers.
    conf_energies: dict.
        dictionary of conformer indexes (keys) and energies (values, KCal/Mol).
    '''

    # Loop over confs.
    conf_energies = {}
    for cid in cids:

        # Minimise at MMFF94s.
        prop = AllChem.MMFFGetMoleculeProperties(mol, mmffVariant="MMFF94s")
        ff = AllChem.MMFFGetMoleculeForceField(mol, prop, confId=cid)
        ff.Minimize()

        # Compute MM energy.
        energy = float(ff.CalcEnergy())
        conf_energies[cid] = energy

    # Discard high energy ones.
    # Note that QM might bring down some high energy ones.
    mol, conf_energies = energy_polish(mol, conf_energies)

    return mol, conf_energies


def energy_polish(mol, conf_energies, thresh=4.0):
    '''
    Function to remove high energy conformers for a molecule and to align them
    to the most stable.

    Parameters
    ----------
    mol: RDKit mol object.
        molecule object containing the conformers to be optimised.
    conf_energies: dict.
        dictionary of conformer indexes (keys) and energies (values, KCal/Mol).
    thresh: float (default: 4.0).
        threshold for energy refinement of conformers.

    Returns
    -------
    mol1: RDKit mol object.
        molecule object with the desired conformers.
    conf_energies1: dict.
        dictionary of conformer indexes (keys) and energies (values, KCal/Mol).
    '''

    # Sort by energy.
    confs_sorted = sorted(conf_energies.items(), key=lambda x: x[1])

    # Set up max energy.
    lowest = confs_sorted[0][1]
    highest = lowest + thresh

    # Create a new mol obj.
    mol1 = Chem.Mol(mol)
    mol1.RemoveAllConformers()

    # Remove high energy conformers.
    clean = []
    for cid, ene in confs_sorted:
        if ene <= highest:
            clean.append((cid, ene))
            mol1.AddConformer(mol.GetConformer(cid))

        # Stop as soon as we surpass the max energy allowed.
        else:
            break

    # Store info in a dict.
    conf_energies1 = OrderedDict(clean)

    # Align by rms.
    AllChem.AlignMolConformers(mol1)

    return mol1, conf_energies1


def write_conformers(mol, cids, filename):
    '''
    Function to write conformers for a molecule to a file.

    Parameters
    ----------
    mol: RDKit mol object.
        molecule object containing the conformers to be optimised.
    cids: list of int.
        conformer indexes relative to mol.
    filename: str.
        name of the file to be written.

    '''

    # Initialise writer.
    w = Chem.SDWriter(filename)

    # Loop over conformers.
    for cid in cids:
        w.write(mol, confId=cid)

    # Close.
    w.flush()
    w.close()

    return


if __name__ == '__main__':

    # Read in the whole library.
    frags = Chem.SmilesMolSupplier('mols.smi')
    fragslist = [ m for m in frags ]
